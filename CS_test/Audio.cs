﻿using System;

namespace CS_test
{
	
	public class Audio
	{
		public string artist;
		public string title;
		public string url;
		
		public int owner_id;
		public int duration;
		public int owned;
		public int Num;
		public int aid;
		
		public Audio()
		{
			artist = "";
			title = "";
			duration = 0;
			url = "";
			aid = 0;
			Num = 0;
			owned = 0;
		}
		
		public override string ToString()
		{
			if(owned==1) { return "[O] "+artist+" — "+title; }
			return artist+" — "+title;
		}
		public string ToStringWO()
		{
			return artist+" — "+title;
		}
		
	}//audio_class
	
	public class Auth_VK
	{
		public string access_token;
		public int expires_in;
		public int user_id;
	}//auth class
	
	public class resp_int
	{
		public int response;
	}
	
	public class err_resp
	{
		public int error_code;
		public string error_msg;
		
		public override string ToString()
		{
			return "Err "+error_code+": "+error_msg;
		}
	}
}
