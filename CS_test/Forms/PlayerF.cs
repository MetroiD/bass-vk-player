﻿using System;
using System.IO;
using System.Windows.Forms;
using System.ComponentModel;



using Un4seen.Bass;
using Un4seen.Bass.AddOn.Vst;

using System.Net;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;

namespace CS_test
{

	public partial class PlayerF : Form
	{
		private Audio CurA;
		private MainForm _Parent;
		private string Status;
		
		//bass
		private bool InitDefaultDevice;
		private const int HZ = 44100;
		private int BASS_str;
		//vst
		private int VST_h;
		private BASS_VST_INFO vstInfo;
		private Form VST_form;
		private float[] VST_Params;
		
		//Delegates
		private delegate void voidDel();
		
		public PlayerF(MainForm p)
		{
			InitializeComponent();
			_Parent = p;
			B_Init(HZ);
			Status = "Stop";
		}
		
		//BASS METODS
		private bool B_Init(int hz)
		{
			if(!InitDefaultDevice)
			{
				InitDefaultDevice =Bass.BASS_Init(-1,hz,BASSInit.BASS_DEVICE_DEFAULT,IntPtr.Zero);
				vstInfo = new BASS_VST_INFO();
			}
			return InitDefaultDevice;
		}
		private void B_play(string UR, int v)
		{
			B_stop(BASS_str);
			int off = 0;
			BASSFlag flags = BASSFlag.BASS_DEFAULT;
			BASS_str = Bass.BASS_StreamCreateURL(UR, off, flags, null, IntPtr.Zero);
			if (BASS_str != 0)
			{
				B_SetVol(v,BASS_str);
				trackBar1.Maximum = B_GetLength(BASS_str);
				timer1.Start();
				VST_h = BassVst.BASS_VST_ChannelSetDSP(BASS_str,"iZotope Ozone 5.dll", BASSVSTDsp.BASS_VST_DEFAULT,0);	
				VST_Load(VST_h);
				Bass.BASS_ChannelPlay(BASS_str,false);
				Status = "Play";
				
			}
			
		}
		private void B_stop(int str)
		{
			Bass.BASS_ChannelStop(str);
			Bass.BASS_StreamFree(str);
			timer1.Stop();
			trackBar1.Maximum = 0;
			TimeSpan time = TimeSpan.FromSeconds(0);
			toolStripStatusLabel1.Text = time.ToString(@"hh\:mm\:ss");
			BASS_str = 0;
			VST_h = 0;
			Status = "Stop";
			if(VST_form!=null) VST_form.Close();
		}
		private void B_SetVol(int v,int str)
		{
			Bass.BASS_ChannelSetAttribute(str,BASSAttribute.BASS_ATTRIB_VOL, v/100f);
		}
		private int  B_GetLength(int str)
		{
			long TimeB = Bass.BASS_ChannelGetLength(str);
			return (int)(Bass.BASS_ChannelBytes2Seconds(str,TimeB));
		}
		private int  B_GetPosStr(int str)
		{
			long TimeP = Bass.BASS_ChannelGetPosition(str);
			return (int)(Bass.BASS_ChannelBytes2Seconds(str,TimeP));
		}
		private void B_SetPos(int str, int sec)
		{
			long ps = Bass.BASS_ChannelSeconds2Bytes(str,(double)sec);
			Bass.BASS_ChannelSetPosition(str,ps);
		}
		private void VST_open()
		{
			if ( BassVst.BASS_VST_GetInfo(VST_h, vstInfo) && vstInfo.hasEditor )
			{
				VST_form = new Form();
				VST_form.Width = vstInfo.editorWidth+4;
				VST_form.Height = vstInfo.editorHeight+4;
				VST_form.Closing += new CancelEventHandler(VST_close);
				VST_form.Text = vstInfo.effectName;
				VST_form.ShowIcon = false;
				//f.FormBorderStyle = FormBorderStyle.None;
				//BassVst.BASS_VST_EmbedEditor(VST_h, f.Handle);
				BassVst.BASS_VST_EmbedEditor(VST_h, VST_form.Handle);
				VST_form.Show();
			}
		}
		private void VST_Save(int han)
		{
			int size = BassVst.BASS_VST_GetParamCount(han);
			VST_Params = new float[size];
			for (int i=0; i<size; i++)
			{ VST_Params[i] = BassVst.BASS_VST_GetParam(han, i); }
			//Debug.WriteLine("Rewrited VST  state");
			
			//save to file
			string path = "VST_CFG.bin";
			FileMode _FM;
			if (!File.Exists(path)) _FM = FileMode.Create;
			else _FM = FileMode.Truncate;
			FileStream FileStream = new FileStream(path, _FM , FileAccess.Write);
			byte[] byteArray = new byte[VST_Params.Length * 4];
			Buffer.BlockCopy(VST_Params, 0, byteArray, 0, byteArray.Length);
			FileStream.Write(byteArray, 0, byteArray.Length);
		    FileStream.Close();
			//Debug.WriteLine("Saved VST  state");
		}
		private void VST_Load(int han) 
		{
			if(VST_Params!=null)
			{
				if(VST_Params.Length==BassVst.BASS_VST_GetParamCount(han))
				{
					for (int i=0; i<BassVst.BASS_VST_GetParamCount(han); i++)
					{
						float p = VST_Params[i];
						BassVst.BASS_VST_SetParam(han, i, p);
					}
					//Debug.WriteLine("Loaded VST state");
				}
			}
			else
			{
				string path = "VST_CFG.bin";
				FileMode _FM;
				if (File.Exists(path)) 
				{
					_FM = FileMode.Open;
					FileStream FStream = new FileStream(path, _FM , FileAccess.Read);
					byte[] byteArray = new byte[2048];
					using (MemoryStream ms = new MemoryStream())
				    {
				        FStream.CopyTo(ms);
				        byteArray = ms.ToArray();
				    }
					FStream.Close();
					VST_Params = new float[512];
					Buffer.BlockCopy(byteArray, 0, VST_Params, 0, byteArray.Length);
					//Debug.WriteLine("VST state loaded from file");
					VST_Load(han);
		        }
			}
		}
		private void VST_close(object sender, CancelEventArgs e)
		{
			BassVst.BASS_VST_EmbedEditor(VST_h, IntPtr.Zero); 
			if(VST_h!=0) VST_Save(VST_h); 
		}
		
		//form metods
		public void ALoad(Audio A)
		{
			if(A != null)
			{
				CurA = A;
				toolStripStatusLabel3.Text = A.ToStringWO();
				//this.Show();
				B_play(CurA.url,  trackBar2.Value);
				TimeSpan time = TimeSpan.FromSeconds(CurA.duration);
				toolStripStatusLabel2.Text = time.ToString(@"hh\:mm\:ss");
			}
		}
		public void frontL()
		{
			if (InvokeRequired)
        	{
        		BeginInvoke(new voidDel(frontL), new object[] {});
        	}
			else
			{
				this.BringToFront();
			}
		}
		
		void PlayerFFormClosed(object sender, FormClosedEventArgs e)
		{
			if(VST_form!=null) VST_form.Close();
			B_stop(BASS_str);
			//_Parent.PlyClosed();
		}
		void Timer1Tick(object sender, EventArgs e) 
		{
			int secs = B_GetPosStr(BASS_str);
			if (secs!=-1) trackBar1.Value = secs;
			TimeSpan time = TimeSpan.FromSeconds(secs);
			toolStripStatusLabel1.Text = time.ToString(@"hh\:mm\:ss");
			if (secs==trackBar1.Maximum)
			{
				timer1.Stop();
				if(checkBox1.Checked) ALoad(CurA);
				else _Parent.PlayNext();
			}
		}
		void TrackBar1MouseDown(object sender, MouseEventArgs e) { timer1.Stop(); }
		void TrackBar1MouseUp(object sender, MouseEventArgs e)
		{
			if(trackBar1.Value!=-1)
			{
				B_SetPos(BASS_str, trackBar1.Value);
				Timer1Tick(new object(),new EventArgs());
				timer1.Start();
			}
		}
		void TrackBar2ValueChanged(object sender, EventArgs e) { B_SetVol(trackBar2.Value,BASS_str); }
		
		//btns
		void Next_BtnClick(object sender, EventArgs e) { _Parent.PlayNext(); }
		void VST_btn_Click(object sender, EventArgs e) { VST_open(); }
		void StopClick(object sender, EventArgs e) { B_stop(BASS_str); }
		void PlayClick(object sender, EventArgs e) 
		{ 
			switch(Status)
			{
				case "Play":
					Bass.BASS_ChannelPause(BASS_str);
					Status = "Pause";
				break;
				case "Pause":
					Bass.BASS_ChannelPlay(BASS_str,false);
					Status = "Play";
				break;
				case "Stop":
					ALoad(CurA);
				break;
					
			}
		}

		
	}
}
