﻿using System;
using System.IO;
using System.Text;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;

namespace CS_test
{
	public partial class MainForm : Form
	{
		//vars
		Thread Send;
		PlayerF Ply;
		Auth_f lform;
		int PlNum;
		private bool authorized_stat;
		private Auth_VK authorized_data;
		
		//hotkey logics
		[System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern bool RegisterHotKey(IntPtr hWnd, int id, int fsModifiers, int vk);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern bool UnregisterHotKey(IntPtr hWnd, int id);
        enum KeyModifier { None = 0, Alt = 1, Control = 2, Shift = 4, WinKey = 8 }
        
		//delegates
		delegate void audDel(List<Audio> A,int CMD);
		delegate void authDel(Auth_VK data);
		delegate void strDel(string s);
		delegate void voidDel();
		
		public MainForm()
		{
			//init
			InitializeComponent();
			authorized_stat = false;
			Send = new Thread( new HTTP( this , Ply ).Tick );
			Send.Start();
			
			Ply = new PlayerF(this);
			Ply.TopLevel = false;
			//Ply.AutoScroll = true;
			panel1.Controls.Add(Ply);
			Ply.FormBorderStyle = FormBorderStyle.None;
			Ply.Show();
			
			
			RegisterHotKey(this.Handle, 0, (int)KeyModifier.Alt, Keys.F2.GetHashCode());
			//done
			status("Inited");
			Autorizate();
		}
		
		//hotkey metod
		protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);
            if (m.Msg == 0x0312)
            {
				Keys key = (Keys)(((int)m.LParam >> 16) & 0xFFFF);                  // The key of the hotkey that was pressed.
				KeyModifier modifier = (KeyModifier)((int)m.LParam & 0xFFFF);       // The modifier of the hotkey that was pressed.
				int id = m.WParam.ToInt32();                                        // The id of the hotkey that was pressed.
				switch(id){ case 0: PlayNext(); break; }
            }
        }
		
		//form metods
		public void addAu(List<Audio> A,int CMD)
		{
			if (InvokeRequired)
        	{
        		BeginInvoke(new audDel(addAu), new object[] {A,CMD});
        	}
        	else
        	{
        		listBox1.Items.Clear();
        		listBox1.BeginUpdate();
        		if(CMD == 4)
        		{
        			for(int i = 0; i < A.Count; i++ )
        			{
        				if(A[i].owner_id==authorized_data.user_id) { A[i].owned=1;}
        			}
        		}
        		
        		for(int i = 0; i < A.Count; i++ )
        		{
        			Audio CA = A[i];
        			CA.Num = listBox1.Items.Count+1;
        			listBox1.Items.Add( CA );
        			//listBox1.Items[i].BackColor = Color.Green;
        		}
        		
        		
        		
        		listBox1.EndUpdate();
        		setL1(listBox1.Items.Count.ToString());
        		status("Audio loaded");
        	}
		}
		public void setL1(string st) { label1.Text = st; }
		public void status(string t) 
		{
			if (InvokeRequired)
        	{
        		BeginInvoke(new strDel(status), new object[] {t});
        	}
			else Status_Text.Text=t; 
		}
		public void AuthClosed() { lform = null; }
		public void PlayNext()
		{
			if(listBox1.Items.Count>0)
			{
				PlNum++;
				listBox1.SetSelected(PlNum,true);
				if(PlNum<=listBox1.Items.Count) Ply.ALoad( ((Audio)listBox1.Items[PlNum]) );
				status("Next track loaded");
			}
		}
		//public void PlyClosed() { Ply = new PlayerF(this); }
		
		void MainFormFormClosing(object sender, FormClosingEventArgs e) { UnregisterHotKey(this.Handle, 0); }
		void mainLB_DouClick(object sender, EventArgs e) 
		{ 
			PlNum = listBox1.SelectedIndex;
			Ply.ALoad( ((Audio)listBox1.SelectedItem) );
			status("Audio loaded");
		}
		void MainFormFormClosed(object sender, FormClosedEventArgs e) { HTTP.Instance.Stop(); }
		void SendSearch()
		{		
			HTTP.Instance.mtx.WaitOne();
			HTTP.Instance.Command.Add( new Object[3] {4,this,SearchBox1.Text} );
			HTTP.Instance.mtx.ReleaseMutex();
			
			status("Search sended");
		}
		
		//btns
		void RandomizeClick(object sender, EventArgs e)
		{
			if(listBox1.Items.Count!=0)
			{
				Random rnd=new Random();
				Audio[] auds = new Audio[listBox1.Items.Count];
	           	listBox1.Items.CopyTo(auds,0);
				Audio[] rand = auds.OrderBy(x => rnd.Next()).ToArray();
				listBox1.Items.Clear();
				listBox1.BeginUpdate();
				for(int i = 0; i < rand.Count(); i++ )
	        	{
	        		Audio CA = rand[i];
	        		listBox1.Items.Add( CA );
	        	}
	        	listBox1.EndUpdate();
	        	PlNum = 0;
	        	status("Randomized");
			} else status("Nothing to randomise");
			
		}
		void ACC_btnClick(object sender, EventArgs e)
		{
			SearchBox1.Text = "";
			if(authorized_stat)
			{
				HTTP.Instance.mtx.WaitOne();
				HTTP.Instance.Command.Add( new Object[3] {1,this,20} );
				HTTP.Instance.mtx.ReleaseMutex();
				status("Get user playlist sended");
			}
			else status("Error: Not Authorized");
		}
		void ListBox1MouseDown(object sender, MouseEventArgs e)
		{
			if(e.Button == MouseButtons.Right)
			{
				listBox1.SelectedIndex = listBox1.IndexFromPoint(e.X, e.Y);
			}
		}
		void Search_BtnClick(object sender, EventArgs e) { SendSearch(); }
		void SearchBox1KeyDown(object sender, KeyEventArgs e) { if(e.KeyCode.ToString()=="Return") { SendSearch(); } }
		void ToolStripMenuItem1Click(object sender, EventArgs e)
		{
			if(listBox1.Items.Count>0)
			{
				Clipboard.SetText(((Audio)listBox1.SelectedItem).ToStringWO()); 
				status("Copy to clipbord done.");
			}
			else status("Nothing to copy");
		} 
		void ToolStripMenuItem2Click(object sender, EventArgs e) 
		{
			if(listBox1.Items.Count>0)
			{
				status("AID - " + ((Audio)listBox1.SelectedItem).aid);
			}
		}
		void ToolStripMenuItem3Click(object sender, EventArgs e)
		{
			if(listBox1.Items.Count>0)
			{
				int t_aid = ((Audio)listBox1.SelectedItem).aid;
				int t_uid = ((Audio)listBox1.SelectedItem).owner_id;
				
				HTTP.Instance.mtx.WaitOne();
				HTTP.Instance.Command.Add( new Object[4] {6,this,t_aid,t_uid} );
				HTTP.Instance.mtx.ReleaseMutex();
				
				status("Add " +t_aid+ " sended");
			}
		}
		void ToolStripMenuItem4Click(object sender, EventArgs e)
		{
			if(listBox1.Items.Count>0)
			{
				int t_aid = ((Audio)listBox1.SelectedItem).aid;
				
				HTTP.Instance.mtx.WaitOne();
				HTTP.Instance.Command.Add( new Object[3] {5,this,t_aid} );
				HTTP.Instance.mtx.ReleaseMutex();
				
				status("Remove " +t_aid+ " sended");
			}
		}
		void ContextMenuStrip1Opening(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if(listBox1.Items.Count!=0)
			{
				if(authorized_data!=null)
				{
					if(((Audio)listBox1.SelectedItem).owner_id==authorized_data.user_id)
					{
						contextMenuStrip1.Items[3].Enabled = true;
					}
					else
					{
						contextMenuStrip1.Items[2].Enabled = true;
					}
				}
			}
		}
		void ContextMenuStrip1Closed(object sender, ToolStripDropDownClosedEventArgs e)
		{
			contextMenuStrip1.Items[2].Enabled = false;
			contextMenuStrip1.Items[3].Enabled = false;
		}
		

		//Auth code
		public void Authorized(Auth_VK data)
		{
			if (InvokeRequired)
        	{
				BeginInvoke(new authDel(Authorized), new object[] {data});
        	}
        	else
        	{
        		authorized_stat = true;
        		authorized_data = data;
        		save_login(data);
        		status("Autorized");
        	}
		}
		public void openAuthForm()
		{
			if (InvokeRequired)
        	{
        		BeginInvoke(new voidDel(openAuthForm), new object[] {});
        	}
        	else
        	{
        		authorized_stat = false;
        		authorized_data = null;
				if(lform==null)
				{
					lform = new Auth_f(this);
					lform.Show();
				}
				else{ lform.Show(); }
        	}
		}
		Auth_VK load_login()
		{
			string path = "login.bin";
			if (File.Exists(path)) 
			{
				Auth_VK data = new Auth_VK();
				FileMode _FM = FileMode.Open;
				FileStream FStream = new FileStream(path, _FM , FileAccess.Read);
				if(FStream.Length > 10)
				{
					byte[] b_buff = new byte[1];
					FStream.Read(b_buff,0,1);
					int t_size = (int)b_buff[0];
					b_buff = new byte[t_size];
					FStream.Read(b_buff,0,t_size);
					data.access_token = Encoding.ASCII.GetString(b_buff);
					b_buff = new byte[20];
					FStream.Read(b_buff,0,20);
					data.user_id = Convert.ToInt32(Encoding.ASCII.GetString(b_buff));
					FStream.Close();
					return data;
				}
				else
				{
					FStream.Close();
					return null;
				}
		        
			}
			else
			{
				return null;
			}
		}
		void Autorizate()
		{
			Auth_VK loaded = load_login();			
			if(loaded!=null) 
			{
				HTTP.Instance.mtx.WaitOne();
				HTTP.Instance.Command.Add( new Object[3] {3,this,loaded} );
				HTTP.Instance.mtx.ReleaseMutex();
			}
			else openAuthForm();
		}
		void Logout_btn_clk(object sender, EventArgs e)
		{
			listBox1.Items.Clear();
			string path = "login.bin";
			if(File.Exists(path)) { File.Delete(path); }
			HTTP.Instance.mtx.WaitOne();
			HTTP.Instance.Command.Add( new Object[2] {3,this} );
			HTTP.Instance.mtx.ReleaseMutex();
		}
		void save_login(Auth_VK _l)
		{
			string path = "login.bin";
			FileMode _FM;
			if (!File.Exists(path)) _FM = FileMode.Create;
			else _FM = FileMode.Truncate;
			FileStream FStream = new FileStream(path, _FM , FileAccess.Write);
			
			byte[] b_token = Encoding.ASCII.GetBytes(_l.access_token);
			byte[] b_size = {(byte)b_token.Length};
			byte[] b_id =  Encoding.ASCII.GetBytes(_l.user_id.ToString());
			
			FStream.Write( b_size, 0, b_size.Length );
			FStream.Write( b_token, 0, b_token.Length );
			FStream.Write( b_id, 0, b_id.Length );
	        FStream.Close();
			
		}
		
		
	}//form
}//namespace
