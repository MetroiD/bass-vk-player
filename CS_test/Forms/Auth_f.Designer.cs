﻿/*
 * Created by SharpDevelop.
 * User: Rysti
 * Date: 08.08.2016
 * Time: 0:19
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace CS_test
{
	partial class Auth_f
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Button ok_bt;
		private System.Windows.Forms.Button canc_bt;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.TextBox textBox2;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.ok_bt = new System.Windows.Forms.Button();
			this.canc_bt = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// ok_bt
			// 
			this.ok_bt.Location = new System.Drawing.Point(12, 72);
			this.ok_bt.Name = "ok_bt";
			this.ok_bt.Size = new System.Drawing.Size(58, 23);
			this.ok_bt.TabIndex = 0;
			this.ok_bt.Text = "Ok";
			this.ok_bt.UseVisualStyleBackColor = true;
			this.ok_bt.Click += new System.EventHandler(this.Ok_btClick);
			// 
			// canc_bt
			// 
			this.canc_bt.Location = new System.Drawing.Point(159, 72);
			this.canc_bt.Name = "canc_bt";
			this.canc_bt.Size = new System.Drawing.Size(58, 23);
			this.canc_bt.TabIndex = 1;
			this.canc_bt.Text = "Cancel";
			this.canc_bt.UseVisualStyleBackColor = true;
			this.canc_bt.Click += new System.EventHandler(this.Canc_btClick);
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1.Location = new System.Drawing.Point(18, 40);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(52, 19);
			this.label1.TabIndex = 2;
			this.label1.Text = "Pass";
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label2.Location = new System.Drawing.Point(18, 9);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(52, 19);
			this.label2.TabIndex = 3;
			this.label2.Text = "Login";
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(106, 9);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(100, 20);
			this.textBox1.TabIndex = 4;
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(106, 39);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(100, 20);
			this.textBox2.TabIndex = 5;
			// 
			// Auth_f
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(229, 107);
			this.Controls.Add(this.textBox2);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.canc_bt);
			this.Controls.Add(this.ok_bt);
			this.HelpButton = true;
			this.MaximumSize = new System.Drawing.Size(245, 146);
			this.MinimumSize = new System.Drawing.Size(245, 146);
			this.Name = "Auth_f";
			this.ShowIcon = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Auth";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Auth_fFormClosing);
			this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Auth_fKeyPress);
			this.ResumeLayout(false);
			this.PerformLayout();

		}
	}
}
