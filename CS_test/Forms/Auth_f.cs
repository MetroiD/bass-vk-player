﻿using System;
using System.Windows.Forms;

namespace CS_test
{
	public partial class Auth_f : Form
	{
		MainForm _Parent;
		public Auth_f(MainForm _f)
		{
			InitializeComponent();
			_Parent = _f;
		}
		void Ok_btClick(object sender, EventArgs e)
		{
			HTTP.Instance.mtx.WaitOne();
			HTTP.Instance.Command.Add( new Object[4] {2,_Parent,textBox1.Text,textBox2.Text} );
			HTTP.Instance.mtx.ReleaseMutex();
			this.Close();
		}
		void Canc_btClick(object sender, EventArgs e)
		{
			this.Close();
		}
		void Auth_fKeyPress(object sender, KeyPressEventArgs e)
		{
			//e.KeyChar
		}
		void Auth_fFormClosing(object sender, FormClosingEventArgs e)
		{
			_Parent.AuthClosed();
		}
	}
}
