﻿/*
 * Created by SharpDevelop.
 * User: Rysti
 * Date: 29.07.2016
 * Time: 16:23
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace CS_test
{
	partial class PlayerF
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.TrackBar trackBar1;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
		private System.Windows.Forms.Button Play;
		private System.Windows.Forms.Button Stop;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.TrackBar trackBar2;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
		private System.Windows.Forms.Button Next_Btn;
		private System.Windows.Forms.Button Open_VST_btn;
		private System.Windows.Forms.CheckBox checkBox1;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel4;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;

		
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// 
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlayerF));
			this.trackBar1 = new System.Windows.Forms.TrackBar();
			this.Play = new System.Windows.Forms.Button();
			this.Stop = new System.Windows.Forms.Button();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.trackBar2 = new System.Windows.Forms.TrackBar();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
			this.Next_Btn = new System.Windows.Forms.Button();
			this.Open_VST_btn = new System.Windows.Forms.Button();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.toolStripStatusLabel4 = new System.Windows.Forms.ToolStripStatusLabel();
			((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.trackBar2)).BeginInit();
			this.statusStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// trackBar1
			// 
			this.trackBar1.AllowDrop = true;
			this.trackBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.trackBar1.Location = new System.Drawing.Point(11, 3);
			this.trackBar1.Maximum = 1;
			this.trackBar1.Name = "trackBar1";
			this.trackBar1.Size = new System.Drawing.Size(546, 45);
			this.trackBar1.TabIndex = 0;
			this.trackBar1.TickFrequency = 0;
			this.trackBar1.TickStyle = System.Windows.Forms.TickStyle.None;
			this.trackBar1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TrackBar1MouseDown);
			this.trackBar1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TrackBar1MouseUp);
			// 
			// Play
			// 
			this.Play.Location = new System.Drawing.Point(11, 28);
			this.Play.Name = "Play";
			this.Play.Size = new System.Drawing.Size(75, 23);
			this.Play.TabIndex = 2;
			this.Play.Text = "Play/Pause";
			this.Play.UseVisualStyleBackColor = true;
			this.Play.Click += new System.EventHandler(this.PlayClick);
			// 
			// Stop
			// 
			this.Stop.Location = new System.Drawing.Point(92, 28);
			this.Stop.Name = "Stop";
			this.Stop.Size = new System.Drawing.Size(75, 23);
			this.Stop.TabIndex = 4;
			this.Stop.Text = "Stop";
			this.Stop.UseVisualStyleBackColor = true;
			this.Stop.Click += new System.EventHandler(this.StopClick);
			// 
			// timer1
			// 
			this.timer1.Interval = 1000;
			this.timer1.Tick += new System.EventHandler(this.Timer1Tick);
			// 
			// trackBar2
			// 
			this.trackBar2.AllowDrop = true;
			this.trackBar2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.trackBar2.Location = new System.Drawing.Point(352, 28);
			this.trackBar2.Maximum = 100;
			this.trackBar2.Name = "trackBar2";
			this.trackBar2.Size = new System.Drawing.Size(205, 45);
			this.trackBar2.TabIndex = 5;
			this.trackBar2.TickFrequency = 0;
			this.trackBar2.TickStyle = System.Windows.Forms.TickStyle.None;
			this.trackBar2.Value = 100;
			this.trackBar2.ValueChanged += new System.EventHandler(this.TrackBar2ValueChanged);
			// 
			// statusStrip1
			// 
			this.statusStrip1.AutoSize = false;
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.toolStripStatusLabel1,
			this.toolStripStatusLabel2,
			this.toolStripStatusLabel4,
			this.toolStripStatusLabel3});
			this.statusStrip1.Location = new System.Drawing.Point(0, 58);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(570, 22);
			this.statusStrip1.SizingGrip = false;
			this.statusStrip1.TabIndex = 6;
			this.statusStrip1.TabStop = true;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// toolStripStatusLabel1
			// 
			this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
			this.toolStripStatusLabel1.Size = new System.Drawing.Size(49, 17);
			this.toolStripStatusLabel1.Text = "00:00:00";
			// 
			// toolStripStatusLabel2
			// 
			this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
			this.toolStripStatusLabel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.toolStripStatusLabel2.Size = new System.Drawing.Size(49, 17);
			this.toolStripStatusLabel2.Text = "00:00:00";
			// 
			// toolStripStatusLabel3
			// 
			this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
			this.toolStripStatusLabel3.Size = new System.Drawing.Size(0, 17);
			// 
			// Next_Btn
			// 
			this.Next_Btn.Location = new System.Drawing.Point(173, 28);
			this.Next_Btn.Name = "Next_Btn";
			this.Next_Btn.Size = new System.Drawing.Size(75, 23);
			this.Next_Btn.TabIndex = 7;
			this.Next_Btn.Text = "Next Track";
			this.Next_Btn.UseVisualStyleBackColor = true;
			this.Next_Btn.Click += new System.EventHandler(this.Next_BtnClick);
			// 
			// Open_VST_btn
			// 
			this.Open_VST_btn.Location = new System.Drawing.Point(254, 28);
			this.Open_VST_btn.Name = "Open_VST_btn";
			this.Open_VST_btn.Size = new System.Drawing.Size(75, 23);
			this.Open_VST_btn.TabIndex = 8;
			this.Open_VST_btn.Text = "VST";
			this.Open_VST_btn.UseVisualStyleBackColor = true;
			this.Open_VST_btn.Click += new System.EventHandler(this.VST_btn_Click);
			// 
			// checkBox1
			// 
			this.checkBox1.Location = new System.Drawing.Point(335, 28);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(15, 24);
			this.checkBox1.TabIndex = 9;
			this.checkBox1.UseVisualStyleBackColor = true;
			// 
			// toolStripStatusLabel4
			// 
			this.toolStripStatusLabel4.Name = "toolStripStatusLabel4";
			this.toolStripStatusLabel4.Size = new System.Drawing.Size(16, 17);
			this.toolStripStatusLabel4.Text = " | ";
			// 
			// PlayerF
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(570, 80);
			this.ControlBox = false;
			this.Controls.Add(this.checkBox1);
			this.Controls.Add(this.Open_VST_btn);
			this.Controls.Add(this.Next_Btn);
			this.Controls.Add(this.statusStrip1);
			this.Controls.Add(this.trackBar2);
			this.Controls.Add(this.Stop);
			this.Controls.Add(this.Play);
			this.Controls.Add(this.trackBar1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MaximumSize = new System.Drawing.Size(570, 80);
			this.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(570, 80);
			this.Name = "PlayerF";
			this.Text = "Ply";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.PlayerFFormClosed);
			((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.trackBar2)).EndInit();
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

			this.Controls.Add(this.trackBar2);
			this.Controls.Add(this.Stop);
			this.Controls.Add(this.Play);
			this.Controls.Add(this.trackBar1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MaximumSize = new System.Drawing.Size(570, 80);
			this.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(570, 80);
			this.Name = "PlayerF";
			this.Text = "Ply";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.PlayerFFormClosed);
			((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.trackBar2)).EndInit();
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
	}
}
