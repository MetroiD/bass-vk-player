﻿
namespace CS_test
{
	partial class MainForm
	{

		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.ToolStripStatusLabel Status_Text;
		private System.Windows.Forms.Button Randomize;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ListBox listBox1;
		private System.Windows.Forms.Button logout_btn;
		private System.Windows.Forms.Button ACC_btn;
		private System.Windows.Forms.TextBox SearchBox1;
		private System.Windows.Forms.Button search_Btn;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
		private System.Windows.Forms.Panel panel1;
		

		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.Status_Text = new System.Windows.Forms.ToolStripStatusLabel();
			this.Randomize = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.listBox1 = new System.Windows.Forms.ListBox();
			this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
			this.logout_btn = new System.Windows.Forms.Button();
			this.ACC_btn = new System.Windows.Forms.Button();
			this.SearchBox1 = new System.Windows.Forms.TextBox();
			this.search_Btn = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.statusStrip1.SuspendLayout();
			this.contextMenuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// statusStrip1
			// 
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.Status_Text});
			this.statusStrip1.Location = new System.Drawing.Point(0, 539);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(594, 22);
			this.statusStrip1.TabIndex = 2;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// Status_Text
			// 
			this.Status_Text.Name = "Status_Text";
			this.Status_Text.Size = new System.Drawing.Size(39, 17);
			this.Status_Text.Text = "Status";
			// 
			// Randomize
			// 
			this.Randomize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.Randomize.Location = new System.Drawing.Point(507, 508);
			this.Randomize.Name = "Randomize";
			this.Randomize.Size = new System.Drawing.Size(75, 23);
			this.Randomize.TabIndex = 4;
			this.Randomize.Text = "Randomize";
			this.Randomize.UseVisualStyleBackColor = true;
			this.Randomize.Click += new System.EventHandler(this.RandomizeClick);
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.label1.Location = new System.Drawing.Point(93, 506);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(408, 21);
			this.label1.TabIndex = 5;
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// listBox1
			// 
			this.listBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
			| System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.listBox1.ContextMenuStrip = this.contextMenuStrip1;
			this.listBox1.FormattingEnabled = true;
			this.listBox1.Location = new System.Drawing.Point(12, 120);
			this.listBox1.Name = "listBox1";
			this.listBox1.Size = new System.Drawing.Size(570, 381);
			this.listBox1.TabIndex = 6;
			this.listBox1.DoubleClick += new System.EventHandler(this.mainLB_DouClick);
			this.listBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ListBox1MouseDown);
			// 
			// contextMenuStrip1
			// 
			this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.toolStripMenuItem1,
			this.toolStripMenuItem2,
			this.toolStripMenuItem3,
			this.toolStripMenuItem4});
			this.contextMenuStrip1.Name = "contextMenuStrip1";
			this.contextMenuStrip1.Size = new System.Drawing.Size(207, 92);
			this.contextMenuStrip1.TabStop = true;
			this.contextMenuStrip1.Closed += new System.Windows.Forms.ToolStripDropDownClosedEventHandler(this.ContextMenuStrip1Closed);
			this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.ContextMenuStrip1Opening);
			// 
			// toolStripMenuItem1
			// 
			this.toolStripMenuItem1.Name = "toolStripMenuItem1";
			this.toolStripMenuItem1.Size = new System.Drawing.Size(206, 22);
			this.toolStripMenuItem1.Text = "Copy Name to Clipboard";
			this.toolStripMenuItem1.Click += new System.EventHandler(this.ToolStripMenuItem1Click);
			// 
			// toolStripMenuItem2
			// 
			this.toolStripMenuItem2.Name = "toolStripMenuItem2";
			this.toolStripMenuItem2.Size = new System.Drawing.Size(206, 22);
			this.toolStripMenuItem2.Text = "Print AID";
			this.toolStripMenuItem2.Click += new System.EventHandler(this.ToolStripMenuItem2Click);
			// 
			// toolStripMenuItem3
			// 
			this.toolStripMenuItem3.Enabled = false;
			this.toolStripMenuItem3.Name = "toolStripMenuItem3";
			this.toolStripMenuItem3.Size = new System.Drawing.Size(206, 22);
			this.toolStripMenuItem3.Text = "Add track";
			this.toolStripMenuItem3.Click += new System.EventHandler(this.ToolStripMenuItem3Click);
			// 
			// toolStripMenuItem4
			// 
			this.toolStripMenuItem4.Enabled = false;
			this.toolStripMenuItem4.Name = "toolStripMenuItem4";
			this.toolStripMenuItem4.Size = new System.Drawing.Size(206, 22);
			this.toolStripMenuItem4.Text = "Remove track";
			this.toolStripMenuItem4.Click += new System.EventHandler(this.ToolStripMenuItem4Click);
			// 
			// logout_btn
			// 
			this.logout_btn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.logout_btn.Location = new System.Drawing.Point(12, 508);
			this.logout_btn.Name = "logout_btn";
			this.logout_btn.Size = new System.Drawing.Size(75, 23);
			this.logout_btn.TabIndex = 7;
			this.logout_btn.Text = "Logout";
			this.logout_btn.UseVisualStyleBackColor = true;
			this.logout_btn.Click += new System.EventHandler(this.Logout_btn_clk);
			// 
			// ACC_btn
			// 
			this.ACC_btn.Location = new System.Drawing.Point(12, 87);
			this.ACC_btn.Name = "ACC_btn";
			this.ACC_btn.Size = new System.Drawing.Size(37, 23);
			this.ACC_btn.TabIndex = 8;
			this.ACC_btn.Text = "ACC";
			this.ACC_btn.UseVisualStyleBackColor = true;
			this.ACC_btn.Click += new System.EventHandler(this.ACC_btnClick);
			// 
			// SearchBox1
			// 
			this.SearchBox1.Location = new System.Drawing.Point(55, 90);
			this.SearchBox1.Name = "SearchBox1";
			this.SearchBox1.Size = new System.Drawing.Size(471, 20);
			this.SearchBox1.TabIndex = 10;
			this.SearchBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SearchBox1KeyDown);
			// 
			// search_Btn
			// 
			this.search_Btn.Location = new System.Drawing.Point(532, 88);
			this.search_Btn.Name = "search_Btn";
			this.search_Btn.Size = new System.Drawing.Size(50, 23);
			this.search_Btn.TabIndex = 11;
			this.search_Btn.Text = "Search";
			this.search_Btn.UseVisualStyleBackColor = true;
			this.search_Btn.Click += new System.EventHandler(this.Search_BtnClick);
			// 
			// panel1
			// 
			this.panel1.Location = new System.Drawing.Point(12, 3);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(570, 80);
			this.panel1.TabIndex = 12;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(594, 561);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.search_Btn);
			this.Controls.Add(this.SearchBox1);
			this.Controls.Add(this.ACC_btn);
			this.Controls.Add(this.logout_btn);
			this.Controls.Add(this.listBox1);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.Randomize);
			this.Controls.Add(this.statusStrip1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MaximumSize = new System.Drawing.Size(610, 600);
			this.MinimumSize = new System.Drawing.Size(610, 600);
			this.Name = "MainForm";
			this.Text = "Bass powered VK";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainFormFormClosing);
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainFormFormClosed);
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.contextMenuStrip1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}
	}
}
