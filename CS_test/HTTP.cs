﻿using System;
using System.Diagnostics;

using System.Threading;
using System.Windows.Forms;
using System.Collections.Generic;

using System.IO;
using System.Net;
using System.Text;

using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CS_test
{
	
	public class HTTP
	{
		public List<Object[]> Command;
		public Mutex mtx;
		private Auth_VK Secret;
		
		private bool _Live = true;
		//private MainForm _Parent;
		
		private static HTTP _instance;
		public static HTTP Instance { get { return _instance; } }	
		
		public HTTP(MainForm f,PlayerF pll)
		{
			if (_instance == null) _instance = this;
			
			Command = new List<Object[]>();
			mtx = new Mutex();
		}
		public void Stop() { _Live = false; }
		
		//COMMANDS
		//
		//Send cmd with
		// HTTP.Instance.mtx.WaitOne();
		// HTTP.Instance.Command.Add( new Object[2] {CMD,sender,arg,arg...} );
		// HTTP.Instance.mtx.ReleaseMutex();
		//
		//List of cmds
		//1: GET audios list; Args(int Count) Return in MainForm.addAu
		//2: GET Auth response; Args(string Login,string Pass) Return in MainForm.Authorized
		//3: SET Secret; Args(Auth_VK Secret) Return in MainForm.Authorized
		//4: GET SEARCH audios list; Args(string Text,bool local) Return in MainForm.addAu
		//5: GET Remove audio; Args(int AID) Return in MainForm.status
		//6: GET Add audio; Args(int AID,int UID)
		//
		
		public void Tick()
		{
			while(_Live)
			{
				if (Command.Count>0)
				{
					mtx.WaitOne();
					Object[] CurComm = Command[0];
					Object sender = CurComm[1];
					switch ((int)CurComm[0])
					{
						case 1:
							if(Secret!=null) 
							{
								List<Audio> Au;
								if(CurComm.Count()==3) 	Au = get_audios((int)CurComm[2], 0);
								else 					Au = get_audios(1,0);
								((MainForm)sender).addAu(Au,(int)CurComm[0]);
							}
							break;
							
						case 2:
							if(CurComm.Count()==4)
							{
								Secret = Send_Auth((string)CurComm[2],(string)CurComm[3]);
								if(Secret!=null) { ((MainForm)sender).Authorized(Secret); }
							}
							
							break;
							
						case 3:
							if(CurComm.Count()==3) 
							{
								Secret = (Auth_VK)CurComm[2];
								((MainForm)sender).Authorized(Secret);
							}
							else 
							{
								Secret = null;
								((MainForm)sender).openAuthForm(); 
							}
							break;	
						case 4:
							if(Secret!=null) 
							{
								List<Audio> Au;
								if(CurComm.Count()==3) 	Au = search_audios((string)CurComm[2]);
								else 					Au = null;
								((MainForm)sender).addAu(Au,(int)CurComm[0]);
							}
							break;
						case 5:
							if(Secret!=null) 
							{
								string s = "";
								if(CurComm.Count()==3) 
								{
									if(remove_audio((int)CurComm[2])==1){s = (int)CurComm[2]+" Removed";}
								}
								if(s==""){s = "Err with audio del";}
								((MainForm)sender).status(s);
							}
							break;
						case 6:
							if(Secret!=null) 
							{
								string s = "";
								if(CurComm.Count()==4) 
								{
									s = add_audio((int)CurComm[2],(int)CurComm[3]).ToString();
								}
								if(s==""){s = "Err with audio del";}else{s+=" Added to playlist";}
								((MainForm)sender).status(s);
							}
							break;
							
						default:
							break;
					}
					Command.Remove(CurComm);
					
					mtx.ReleaseMutex();
				}
				Thread.Sleep(250);
			}
		}


		private Auth_VK Send_Auth(string Login,string Pass)
		{
			string Req = "https://oauth.vk.com/token?grant_type=password";
			Req+="&client_id=" + "2685278";
			Req+="&client_secret=" + "AlVXZFMUqyrnABp8ncuU"; // Kate 2685278 "hHbJug59sKJie78wjrH8"      Winvk 3697615 "AlVXZFMUqyrnABp8ncuU"
            Req +="&username=" + Login;
			Req+="&password=" + Pass;
			Req+="&v=5.53&";


               //public class KateConstants {
               //public static String API_ID = "2685278";
               //public static String tmp = "hHbJug59sKJie78wjrH8";
               //req
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Req);
			try
			{
				String RESP = GET_rq(Req);
				return JsonConvert.DeserializeObject<Auth_VK>(RESP);
			}
			catch(Exception e){MessageBox.Show(e.Message); return null;}
			

		}
		private List<Audio> get_audios(int coun,int offset)
		{
			string URL = "https://api.vk.com/method/audio.get";
			URL+="?owner_id="+Secret.user_id;
			URL+="&album_id=";
			URL+="&audio_ids=";
			URL+="&need_user=0";
			URL+="&offset="+offset;
			URL+="&count="+coun;
			URL+="&access_token="+Secret.access_token;
			
			string rp = GET_rq(URL);
			return audio_parser(rp);

		}//End of GetAud
		private List<Audio> search_audios(string req)
		{
			string URL = "https://api.vk.com/method/audio.search";
			URL+="?q="+req;
			URL+="&auto_complete=";
			URL+="&lyrics=";
			URL+="&performer_only=";
			URL+="&sort=2"; //Sort: 2 — ord. popular, 1 — length, 0 — date. 
			URL+="&search_own=1";
			URL+="&offset=";
			URL+="&count="+300;
			URL+="&access_token="+Secret.access_token;
			
			string rp = GET_rq(URL);
			return audio_parser(rp);
		}//End of SearchAud
		private int add_audio(int AID,int UID)
		{
			string URL = "https://api.vk.com/method/audio.add";
			URL+="?audio_id="+AID;
			URL+="&owner_id="+UID;
			URL+="&group_id=";
			URL+="&album_id=";
			URL+="&access_token="+Secret.access_token;
			
			return response_parser(GET_rq(URL));
		}//End of Add AU
		private int remove_audio(int AID)
		{
			string URL = "https://api.vk.com/method/audio.delete";
			URL+="?audio_id="+AID;
			URL+="&owner_id="+Secret.user_id;
			URL+="&access_token="+Secret.access_token;
			
			return response_parser(GET_rq(URL));
		}//End of RM AU
		
		private string GET_rq(string URL)
		{
            Console.WriteLine(URL);
			WebRequest request = WebRequest.Create(URL);
			WebResponse response = request.GetResponse();
			Stream S = response.GetResponseStream();
			Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
			StreamReader readSt = new StreamReader( S, encode );
			Char[] buff = new Char[256];
			int count = readSt.Read( buff, 0, 256 );
			String RESP = "";
			while (count > 0) 
			{
				String str = new String(buff, 0, count);
				RESP += str;
				count = readSt.Read(buff, 0, 256);
			}
			return RESP;
		}//End of GET
		private void POST_rq()
		{
			//WebRequest request = WebRequest.Create("http://google.com");
			//request.Credentials = CredentialCache.DefaultCredentials;
			//((HttpWebRequest)request).UserAgent = ".NET Framework Example Client";
			//request.Method = "POST";
			//request.ContentLength = byteArray.Length;
			//request.ContentType = "application/x-www-form-urlencoded";
			//Stream dataStream = request.GetRequestStream ();
			//dataStream.Write (byteArray, 0, byteArray.Length);
			//dataStream.Close ();
		}//End of POST
		private List<Audio> audio_parser(string S)
		{
			
			JObject R2 = JObject.Parse(S);
			string Rtype = R2.First.Path.ToString();
			IList<JToken> results = R2[Rtype].Children().ToList();
			if (Rtype == "response")
			{
				List<Audio> Audios = new List<Audio>();
				foreach (JToken result in results)
				{
					Audio cur_A = new Audio();
					if(result is JObject)
					{
						cur_A = (Audio)JsonConvert.DeserializeObject<Audio>(result.ToString());
						Audios.Add(cur_A);
					}
				}
				
				return Audios;
				
			}//if returned is resp
			else
			{
				if (Rtype == "error")
				{
					string SS = "{"+results[0]+","+results[1]+"}";
					err_resp err = JsonConvert.DeserializeObject<err_resp> (SS);
					MessageBox.Show(err.ToString());
					return new List<Audio>();
				}
			}//if error(DO SOMETHING, PLEASE, BOSS!!!HDELKJDGSBHvlkdfg)
			
			return new List<Audio>();
			
		}//End of AU parser
		private int response_parser(string S)
		{
			JObject R2 = JObject.Parse(S);
			string Rtype = R2.First.Path.ToString();
			IList<JToken> results = R2[Rtype].Children().ToList();
			if (Rtype == "response") 
			{ 
				resp_int r = JsonConvert.DeserializeObject<resp_int>(S);
				return r.response; 
			}//if returned is resp INT
			else
			{
				if (Rtype == "error")
				{
					string SS = "{"+results[0]+","+results[1]+"}";
					err_resp err = JsonConvert.DeserializeObject<err_resp> (SS);
					MessageBox.Show(err.ToString());
					return 0;
				}
			}//if error(DO SOMETHING, PLEASE, BOSS!!!HDELKJDGSBHvlkdfg)
			
			return 0;
			
		}//Ent of INT Parser
	}
	
	
	
}//namespace
